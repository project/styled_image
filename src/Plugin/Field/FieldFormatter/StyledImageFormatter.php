<?php

namespace Drupal\styled_image\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;

/**
 * Plugin implementation of the 'styled_image_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "styled_image_formatter",
 *   label = @Translation("Styled image"),
 *   field_types = {
 *     "styled_image"
 *   }
 * )
 */
class StyledImageFormatter extends ImageFormatterBase {

  /**
   * Gets the image link options.
   *
   * @return array
   *   The available options array.
   */
  public static function getImageLinkOptions() {
    return [
      'content' => t('Content'),
      'file' => t('File'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_link' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['image_link'] = [
      '#type' => 'select',
      '#title' => $this->t('Link image to'),
      '#options' => self::getImageLinkOptions(),
      '#empty_option' => $this->t('Nothing'),
      '#default_value' => $this->getSetting('image_link'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($image_link = $this->getSetting('image_link')) {
      $options = self::getImageLinkOptions();
      $image_link = $options[$image_link];
    }
    else {
      $image_link = $this->t('Nothing');
    }

    $summary[] = $this->t('Link image to: @image_link', [
      '@image_link' => $image_link,
    ]);

    return $summary;
  }

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for $items, as an array of child elements keyed by
   *   consecutive numeric indexes starting from 0.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    $url = NULL;
    $image_link_setting = $this->getSetting('image_link');
    // Check if the formatter involves a link.
    if ($image_link_setting == 'content') {
      $entity = $items->getEntity();
      if (!$entity->isNew()) {
        $url = $entity->toUrl();
      }
    }
    elseif ($image_link_setting == 'file') {
      $link_file = TRUE;
    }

    foreach ($files as $delta => $file) {
      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $image_style_setting = $item->image_style;

      // Collect cache tags to be added for each item in the field.
      $base_cache_tags = [];
      if (!empty($image_style_setting)) {
        $image_style = \Drupal::entityTypeManager()
          ->getStorage($item->image_style_type)
          ->load($image_style_setting);
        $base_cache_tags = $image_style->getCacheTags();
      }

      $cache_contexts = [];
      if (isset($link_file)) {
        $image_uri = $file->getFileUri();
        // @todo Wrap in file_url_transform_relative(). This is currently
        // impossible. As a work-around, we currently add the 'url.site' cache
        // context to ensure different file URLs are generated for different
        // sites in a multisite setup, including HTTP and HTTPS versions of the
        // same site. Fix in https://www.drupal.org/node/2646744.
        $url = Url::fromUri(file_create_url($image_uri));
        $cache_contexts[] = 'url.site';
      }
      $cache_tags = Cache::mergeTags($base_cache_tags, $file->getCacheTags());

      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      switch ($item->image_style_type) {
        case 'image_style':
          $elements[$delta] = array(
            '#theme' => 'image_formatter',
            '#item' => $item,
            '#item_attributes' => $item_attributes,
            '#image_style' => $image_style_setting,
            '#url' => $url,
            '#cache' => array(
              'tags' => $cache_tags,
              'contexts' => $cache_contexts,
            ),
          );
          break;
        case 'responsive_image_style':
          $elements[$delta] = array(
            '#theme' => 'responsive_image_formatter',
            '#item' => $item,
            '#item_attributes' => $item_attributes,
            '#responsive_image_style_id' => $image_style_setting,
            '#url' => $url,
            '#cache' => array(
              'tags' => $cache_tags,
              'contexts' => $cache_contexts,
            ),
          );
          break;
      }
    }

    return $elements;
  }

}
